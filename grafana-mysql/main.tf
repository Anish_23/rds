provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_db_instance" "grafana_mysql" {
  allocated_storage    = var.size
  engine               = var.engine
  engine_version       = var.engine_version
  instance_class       = var.instance_class
  parameter_group_name = var.parameter_group_name
  skip_final_snapshot  = true
  username             = var.username
  deletion_protection  = var.deletion_protection
  monitoring_interval  = 30
  publicly_accessible  = true
}
