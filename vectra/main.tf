provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_db_instance" "vectra" {
  allocated_storage     = var.size
  engine                = var.engine
  engine_version        = var.engine_version
  instance_class        = var.instance_class
  parameter_group_name  = var.parameter_group_name
  skip_final_snapshot   = true
  username              = var.username
  deletion_protection   = var.deletion_protection
  monitoring_interval   = 0
  publicly_accessible   = true
  copy_tags_to_snapshot = true
  max_allocated_storage = 1000
}
