variable "profile" {
  default     = "default"
  type        = string
  description = "AWS Profile"
}

variable "region" {
  default     = "us-east-1"
  type        = string
  description = "Define the AWS Region"
}

variable "size" {
  default     = 10
  type        = number
  description = "Allocated Storage for the DB"
}

variable "engine" {
  default     = "mysql"
  type        = string
  description = "Engine type for the DB"
}

variable "engine_version" {
  default     = "5.7.33"
  type        = string
  description = "Engine version for the DB"
}

variable "instance_class" {
  default     = "db.t2.small"
  type        = string
  description = "Define the instance class for the DB"
}

variable "parameter_group_name" {
  default     = "default.mysql5.7"
  type        = string
  description = "parameter group name for the DB"
}

variable "username" {
  default     = "admin"
  type        = string
  description = "The username for the DB"
}

variable "deletion_protection" {
  default = false
  type = bool
}
